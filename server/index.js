const express = require("express");
const app = express();
app.use(express.static("../irving-cesar/build"));
app.listen(9873, () => console.log("Irving Cesar Example app on port 9873!"));
